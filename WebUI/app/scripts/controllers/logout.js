'use strict';

angular.module('SPAAuthenticationExampleApp')
  .controller('LogoutCtrl', ['$state', 'User', function ($state, User) {
    User.removeAuthentication();
    $state.go('main');
  }]);
