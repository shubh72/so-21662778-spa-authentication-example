namespace Antaramian.SPAAuthenticationExample.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Antaramian.SPAAuthenticationExample.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Collections.Generic;

    /// <summary>
    /// Class handling configuration of migrations
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<ExampleContext>
    {
        /// <summary>
        /// Migrations configuration constructor
        /// </summary>
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        /// <summary>
        /// Adds a default "admin" user to the database with a password of "password"
        /// </summary>
        /// <param name="context">A datastore access context</param>
        /// <returns>Whether adding the user exists in the database</returns>
        private bool AddUser(ExampleContext context)
        {
            // Identity result objects handle results from non-select operations
            // on the user datastore
            IdentityResult identityResult;
            // UserManager handles the management of a certain type of User, and it
            // requires a UserStore to handle the actual access to the datastore
            UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(context));
            // Create a new user as a POCO
            var user = new IdentityUser()
            {
                UserName = "admin"
            };
            // Check if the user already exists. If so, the user exists so report true
            if (userManager.FindByName(user.UserName) != null)
            {
                return true;
            }
            // Since the user does not exist, attempt to create the user
            identityResult = userManager.Create(user, "password");
            // Pass back the result of attempting to create the user
            return identityResult.Succeeded;
        }

        /// <summary>
        /// Add some default sales data to the databse
        /// </summary>
        /// <param name="context">A datastore access context</param>
        private void AddSales(ExampleContext context)
        {
            // Grab some default regions
            Region northAmerica = context.Regions.Single(r => r.Name == "North America");
            Region europe = context.Regions.Single(r => r.Name == "Europe");
            // Create some default sales (amounts need to be unique)
            List<Sale> sales = new List<Sale> {
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 500m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 200.58m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 300.75m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 1300.45m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 5000.75m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 800.46m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 400.10m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 1070.67m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 657.25m
                },
                new Sale {
                    RegionId = northAmerica.Id,
                    Amount = 142.25m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 1000.46m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 102.15m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 500.46m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 400.00m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 360.00m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 780.00m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 605.67m
                },
                new Sale {
                    RegionId = europe.Id,
                    Amount = 455.42m
                }
            };
            // Add the sales to the database (unless the amount exists)
            sales.ForEach(sale => context.Sales.AddOrUpdate(s => s.Amount, sale));
        }

        /// <summary>
        /// Function that will be run on upwards migrations of the database
        /// </summary>
        /// <param name="context">A datastpre access context</param>
        protected override void Seed(ExampleContext context)
        {
            // Add a default user to the database
            AddUser(context);
            // Define some default regions
            List<Region> regions = new List<Region> {
                new Region {
                    Name = "North America",
                    SalesTarget = 9000m
                },
                new Region
                {
                    Name = "Europe",
                    SalesTarget = 6000m
                }
            };
            // Add them to the context, unless they already exist
            regions.ForEach(region => context.Regions.AddOrUpdate(r => r.Name, region));
            // Then save them to the database
            context.SaveChanges();
            // Now grab the database versions
            Region northAmerica = context.Regions.Single(r => r.Name == "North America");
            Region europe = context.Regions.Single(r => r.Name == "Europe");
            // Define some default employees
            List<Employee> employees = new List<Employee> {
                new Employee
                {
                    FirstName = "Sarah",
                    LastName = "Doe",
                    RegionId = northAmerica.Id
                },
                new Employee
                {
                    FirstName = "John Q.",
                    LastName = "Public",
                    RegionId = europe.Id
                }
            };
            // Add them to the context, unless they already exist
            employees.ForEach(employee => context.Employees.AddOrUpdate(e => e.LastName, employee));
            // Then save them to the database
            context.SaveChanges();
            // Now grab the database versions
            Employee sarahDoe = context.Employees.Single(e => e.FirstName == "Sarah" && e.LastName == "Doe");
            Employee johnPublic = context.Employees.Single(e => e.FirstName == "John Q." && e.LastName == "Public");
            // And make them sales directors
            northAmerica.SalesDirector = sarahDoe;
            europe.SalesDirector = johnPublic;
            // Save their "promotions" to the database
            context.SaveChanges();
            // if there aren't any sales in the database currently, add some
            if (context.Sales.Count() == 0)
            {
                AddSales(context);
            }
            // save any sales that were added
            context.SaveChanges();
        }
    }
}